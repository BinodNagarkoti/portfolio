import React from 'react';
import logo from './logo.svg';
import './App.css';
import { BrowserRouter as Router } from 'react-router-dom'
import Footer from "./component/footer"
import Routes from "./route"
import Navigation from './component/navigation';
function App() {
  return (
<div classname="App">
<Router>
        <Navigation />
    
      
<Routes />


      </Router>
      <Footer/>
      </div>
  );
}

export default App;
