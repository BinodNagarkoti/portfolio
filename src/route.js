import React from 'react';
import {  Route } from 'react-router-dom'
import  Profile  from "./component/profile";
import Contact from "./component/contact"
const Routes = (props) => {
    return ( <div>
        <Route exact path="/" component={Profile} />
        <Route path="/contact" component={Contact} />
        {/* <Route path="/project" /> */}
        {/* <Route path="/" /> */}
    </div> );
}
 
export default Routes;