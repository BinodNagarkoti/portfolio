import React from 'react';
const contact = (props) => {
    return ( <div className="contact">
        <h4> Binod Nagarkoti </h4>
        <span>Info Links:</span>
        <div><a href="https://gitlab.com/BinodNagarkoti" target="_blank">Gitlab</a></div>
       <div> <a href="https://www.linkedin.com/in/binod-nagarkoti-496245128/" target="_blank" >LinkedIn</a></div>
       <div> <a href="https://www.facebook.com/Binodb" target="_blank">Facebook</a></div>
    </div> );
}
 
export default contact;