import React from 'react';
import { Link } from "react-router-dom";
const Navigation = (props) => {
    return ( 
        <div className="navbar">
            
            <ul>
  <li><Link to="/" >Profile</Link></li>
  <li><Link to="/Contact" >Contact</Link></li>
  <li class="dropdown">
    <a href="javascript:void(0)" class="dropbtn">Project</a>
    <div class="dropdown-content">
    <a href="https://gitlab.com/BinodNagarkoti/firstproject" target="_blank">First Project</a>
  <a href="https://gitlab.com/BinodNagarkoti/counters" target="_blank">Counter</a>
  <a href="https://gitlab.com/BinodNagarkoti/todolist" target="_blank">TodoList</a>
  <a href="https://gitlab.com/BinodNagarkoti/todolist2" target="_blank">TodoList-API</a>
  <a href="https://gitlab.com/BinodNagarkoti/contactform" target="_blank">Form</a>
  <a href="https://gitlab.com/BinodNagarkoti/form2" target="_blank">Form 2.0</a>
  <a href="https://gitlab.com/BinodNagarkoti/profile"target="_blank">Profile</a>
  <a href="https://gitlab.com/BinodNagarkoti/weatherapp" target="_blank">Weather-App</a>
  <a href="https://gitlab.com/BinodNagarkoti/basic_route" target="_blank">Basic-Rout</a>
    </div>
  </li>
</ul>

</div>
            
            
          

     );
}
 
export default Navigation;